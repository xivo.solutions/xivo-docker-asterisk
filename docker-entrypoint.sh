#!/bin/bash


while true; do
	if [ $(xivo-confgen asterisk/pjsip.conf 2>/dev/null | wc -l) -gt 0 ]; then
		break;
	fi
	echo "Waiting for confgend" >&2
	sleep 5
done

name="${ASTERISK_HOSTNAME:-asterisk}"

/usr/sbin/ast_tls_cert -C "$name" -O "$name" -d /etc/asterisk/keys

/usr/bin/xivo-sysconfd
exec asterisk -vvvdddf
