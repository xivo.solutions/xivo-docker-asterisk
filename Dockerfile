FROM python:3.9-bullseye


ADD http://mirror.xivo.solutions/xivo_current.key /tmp
ADD xivo-dist.list /etc/apt/sources.list.d/



RUN apt-key add /tmp/xivo_current.key

RUN   apt-get -yq update \
   && apt-get -yqq install \
      libldap2-dev \
      libsasl2-dev \
      libssl-dev \
      git \
      gcc \
      asterisk \
      xivo-sysconfd \
      odbc-postgresql \
      iproute2
      
# these packages expect user astersik and group www-dat without any good reason

RUN apt-get -yqq install \
      asterisk-moh-opsound-g722 \
      asterisk-moh-opsound-gsm \
      asterisk-moh-opsound-wav \
      asterisk-sounds-main \
      asterisk-sounds-wav-en-us \
      asterisk-sounds-wav-fr-fr \
   && apt-get -q autoremove
	




RUN rm -rf /etc/asterisk
RUN mkdir -p /usr/local/src
WORKDIR /usr/local/src
# First copy requirements only to use pip cache if --no-cache-dir option is not used
COPY requirements.txt .
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt && \
	ln -s /usr/local/bin/xivo-confgen /usr/bin

ADD docker-entrypoint.sh /
ADD etc /etc
ADD xivo-config /usr/share/xivo-config
RUN mkdir /etc/asterisk/keys && chown asterisk:asterisk /etc/asterisk/keys

# install xivo-container-helper
WORKDIR /usr/share
ADD https://gitlab.com/xivo.solutions/xivo-container-helper/-/archive/master/xivo-container-helper-master.tar.gz .
RUN     tar -xzf xivo-container-helper-master.tar.gz
RUN     rm xivo-container-helper-master.tar.gz 
RUN     ln -s /usr/share/xivo-container-helper-master/xivo-entrypoint.sh / 
RUN     mkdir /xivo-entrypoint.d

# init entrypoint scripts
RUN ln -s /usr/share/xivo-container-helper-master/xivo-entrypoint.d/templates.sh /xivo-entrypoint.d/10-templates.sh && \
    ln -s /usr/share/xivo-container-helper-master/xivo-entrypoint.d/addip.sh /xivo-entrypoint.d/20-addip.sh && \
    ln -s /docker-entrypoint.sh /xivo-entrypoint.d/90-entrypoint-orig.sh


EXPOSE 2000 5038 5039 5040 
EXPOSE 5060/udp
EXPOSE 11000-12000/udp

VOLUME /etc/asterisk

ENTRYPOINT ["/xivo-entrypoint.sh"]
